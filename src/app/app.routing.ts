import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './webapps/wrapper/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'apps',
    loadChildren: () => import('./webapps/webapps.module').then(m => m.WebappsModule)
  },
  {
    path: 'admin',
    //canActivate: [AuthGuard],
    loadChildren: () => import('./webadmin/webadmin.module').then(m => m.WebadminModule)
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '',
    redirectTo: 'apps',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
