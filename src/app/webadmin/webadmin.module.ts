import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WrapperComponent } from './wrapper/wrapper.component';
import { UsersComponent } from './wrapper/users/users.component';
import { ProfilesComponent } from './wrapper/profiles/profiles.component';
import { SidebarComponent } from './wrapper/sidebar/sidebar.component';
import { NavbarComponent } from './wrapper/navbar/navbar.component';
import { FooterComponent } from './wrapper/footer/footer.component';
import { IncubatorComponent } from './wrapper/incubator/incubator.component';
import { SamplesComponent } from './wrapper/samples/samples.component';
import { DashboardComponent } from './wrapper/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { WebadminRouting } from './webadmin.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MuestraPipe } from './wrapper/pipes/muestra.pipe';
import { IncubadoraPipe } from './wrapper/pipes/incubadora.pipe';
import { UsuariosPipe } from './wrapper/pipes/usuarios.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { CuSampleComponent } from './wrapper/samples/cu-sample/cu-sample.component';
import { CuIncubatorComponent } from './wrapper/incubator/cu-incubator/cu-incubator.component';
import { CuUserComponent } from './wrapper/users/cu-user/cu-user.component';



@NgModule({
  declarations: [
    WrapperComponent,
    UsersComponent,
    ProfilesComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    IncubatorComponent,
    SamplesComponent,
    DashboardComponent,
    UsuariosPipe,
    IncubadoraPipe,
    MuestraPipe,
    CuSampleComponent,
    CuIncubatorComponent,
    CuUserComponent
  ],
  imports: [
    CommonModule,
    WebadminRouting,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ]
})
export class WebadminModule { }
