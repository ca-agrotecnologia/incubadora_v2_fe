import { Component } from '@angular/core';
import { IncubatorService } from '../services/incubator.service';
import { AuthService } from 'src/app/_guards/auth.service';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-incubator',
  templateUrl: './incubator.component.html',
  styleUrls: ['./incubator.component.css'],
})
export class IncubatorComponent {
  public fecha = new Date();
  public showModal = false;
  public incubadoras: any;
  public uSession: any;
  public iSelect: any;
  public btnOpcion = 0;
  public pageSize = 5;
  public page: number = 1;
  public btnBuscar: string = 'Buscar';
  public txtBuscar: string = '';
  public auxBuscar: string = '';
  public numIncubadoras: number = 0;
  public AuxEstatus = -1;

  constructor(private iSer: IncubatorService, private aSer: AuthService) {}

  ngOnInit(): void {
    this.uSession = this.aSer.onGetUsuario();
    this.onGetIncubadoras();
  }

  onGetIncubadoras() {
    this.iSer
      .getIncubadoras(this.uSession.Token)
      .subscribe((resp: RespModel) => {
        if (resp.Estatus) {
          this.incubadoras = resp.Data;
          this.numIncubadoras = this.incubadoras.length;
        }
      });
  }

  onCambiarPermiso() {
    this.iSelect.Estatus = this.AuxEstatus;
    this.iSer
      .putEstatusIncubadora({
        Opcion: 0,
        Token: this.uSession.Token,
        Incubadora: this.iSelect,
      })
      .subscribe((resp: RespModel) => {
        if (resp.Estatus) {
          this.showModal = false;
          this.onGetIncubadoras();
        }
      }, (error) => {
        console.log(error.message);
      });
  }

  onEliminarUsuario() {
    this.iSer
      .eliminarIncubadora(this.iSelect.IncubadoraID, this.uSession.Token)
      .subscribe((resp: RespModel) => {
        console.log(resp);

        if (resp.Estatus) {
          this.showModal = false;
          this.onGetIncubadoras();
        }
      });
  }

  ngOnBuscar() {
    if (this.btnBuscar.length == 6) {
      this.btnBuscar = 'Cancelar';
      this.txtBuscar = this.auxBuscar;
    } else {
      this.btnBuscar = 'Buscar';
      this.txtBuscar = '';
      this.auxBuscar = '';
    }
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

  ngOnModalOpc(Item: any, Opcion: number): void {
    this.btnOpcion = Opcion;
    this.iSelect = Item;
    this.AuxEstatus = Item.Estatus;
    this.showModal = true;
  }
}
