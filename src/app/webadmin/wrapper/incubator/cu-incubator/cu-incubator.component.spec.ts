import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuIncubatorComponent } from './cu-incubator.component';

describe('CuIncubatorComponent', () => {
  let component: CuIncubatorComponent;
  let fixture: ComponentFixture<CuIncubatorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CuIncubatorComponent]
    });
    fixture = TestBed.createComponent(CuIncubatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
