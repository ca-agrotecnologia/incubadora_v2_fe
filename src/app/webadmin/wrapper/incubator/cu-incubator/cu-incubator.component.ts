import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InstitucionModel } from '../../models/institucion.model';
import { ActivatedRoute } from '@angular/router';
import { IncubatorService } from '../../services/incubator.service';
import { AuthService } from 'src/app/_guards/auth.service';
import { RespModel } from '../../models/resp.model';

@Component({
  selector: 'app-cu-incubator',
  templateUrl: './cu-incubator.component.html',
  styleUrls: ['./cu-incubator.component.css'],
})
export class CuIncubatorComponent implements OnInit {
  public formIncubadora!: FormGroup;
  public instituciones: InstitucionModel[] = [];
  public FechaNueva = new Date();
  public modal = {
    titulo: '',
    msg: '',
  };
  public showModal = false;
  public sUsuario: any;
  public Opcion = 0;
  public auxCorreo = '';
  public uSession: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private iSer: IncubatorService,
    private aServicio: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.uSession = this.aServicio.onGetUsuario();
    this.route.params.subscribe((params) => {
      this.Opcion = params['Opcion'];
      if (params['Opcion'] == 3) {
        this.iSer
          .getIncubadora(params['IncubadoraID'], this.uSession.Token)
          .subscribe((resp: RespModel) => {
            if (resp.Estatus) {
              this.ngOnAsignarForm(resp.Data);
            }
          });
      }
    });
    this.iSer.getInstituciones().subscribe(
      (resp: RespModel) => {
        if (resp.Estatus) {
          this.instituciones = resp.Data;
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  private buildForm() {
    this.formIncubadora = this.formBuilder.group({
      IncubadoraID: '',
      InstitucionID: [-1, [Validators.required]],
      UsuarioID: '',
      Nombre: ['', [Validators.required]],
      Capacidad: ['', [Validators.required]],
      Sensores: ['', [Validators.required]],
    });
  }

  onGuardarIncubadora() {
    if (this.formIncubadora.value['InstitucionID'] != -1) {
      this.formIncubadora.controls['UsuarioID'].setValue(
        this.uSession.UsuarioID
      );
      this.iSer
        .postIncubadora({
          Token: this.uSession.Token,
          Incubadora: this.formIncubadora.value,
        })
        .subscribe(
          (resp: RespModel) => {
            console.log(resp);

            if (resp.Estatus) {
              this.modal.titulo = 'Notificación...';
              this.modal.msg = resp.Data;
              this.showModal = true;
              this.formIncubadora.reset();
              this.formIncubadora.controls['InstitucionID'].setValue(-1);
            }
          },
          (error) => {
            console.log(error.message);
          }
        );
    } else {
      this.modal.titulo = 'Error...';
      this.modal.msg = 'Existen datos en blanco';
      this.showModal = true;
    }
  }

  onEditarActividad() {
    if (this.formIncubadora.value['Institucion'] != -1) {
      this.iSer
        .putEstatusIncubadora({
          Opcion: 1,
          Token: this.uSession.Token,
          Incubadora: this.formIncubadora.value,
        })
        .subscribe((resp: RespModel) => {
          if (resp.Estatus) {
            this.modal.titulo = 'Notificación...';
            this.modal.msg = resp.Data;
            this.showModal = true;
          } else {
            this.modal.titulo = 'Notificación...';
            this.modal.msg = resp.Data;
            this.showModal = true;
          }
        });
    } else {
      this.modal.titulo = 'Error...';
      this.modal.msg = 'Existen datos en blanco';
      this.showModal = true;
    }
  }

  ngOnAsignarForm(item: any): void {
    this.formIncubadora.controls['IncubadoraID'].setValue(item.IncubadoraID);
    this.formIncubadora.controls['InstitucionID'].setValue(item.InstitucionID * 1);
    this.formIncubadora.controls['UsuarioID'].setValue(item.UsuarioID);
    this.formIncubadora.controls['Nombre'].setValue(item.Nombre);
    this.formIncubadora.controls['Capacidad'].setValue(item.Capacidad);
    this.formIncubadora.controls['Sensores'].setValue(item.Sensores);
    this.FechaNueva = item.FechAlta;
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }
}
