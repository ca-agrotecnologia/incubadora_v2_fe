import { Component } from '@angular/core';
import { UsersService } from '../services/users.service';
import { AuthService } from 'src/app/_guards/auth.service';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  public fecha = new Date;
  public showModal = false;
  public usuarios: any;
  public uSession: any;
  public iSelect: any;
  public btnOpcion = 0;
  public pageSize = 5;
  public page: number = 1;
  public btnBuscar: string = "Buscar";
  public txtBuscar: string = "";
  public auxBuscar: string = "";
  public numUsuarios: number | undefined;

  constructor(
    private uSer: UsersService,
    private aSer: AuthService
  ) { }

  ngOnInit(): void {
    this.uSession = this.aSer.onGetUsuario();
    this.onGetUsuarios();
  }

  onGetUsuarios() {
    this.uSer.getUsuarios(this.uSession.InstitucionID, this.uSession.Token).subscribe((resp: RespModel) => {
      if (resp.Estatus) {
        this.usuarios = resp.Data;
        this.numUsuarios = this.usuarios.length;
      }
    }, (error) => {
      console.log(error.message);
    });
  }

  onCambiarPermiso() {
    this.iSelect.Estatus = !this.iSelect.Estatus;
    this.uSer.putEstatusUsuario({ Opcion: 0, Token: this.uSession.Token, Usuario: this.iSelect }).subscribe((resp: RespModel) => {
      this.showModal = false;
      if (resp.Estatus) {
        this.onGetUsuarios();
      }
    }, (error) =>{
      console.log(error.message);
    });
  }
  onLimpiarToken() {
    this.uSer.putTokenUsuario({ Opcion: 1, Token: this.uSession.Token, Usuario: this.iSelect }).subscribe((resp: RespModel) => {
      if (resp.Estatus) {
        this.showModal = false;
        this.onGetUsuarios();
      }
    }, (error) =>{
      console.log(error.message);
    });
  }

  onEliminarUsuario() {
    this.uSer.eliminarUsuario(this.iSelect.UsuarioID, this.uSession.Token).subscribe((resp: any) => {
      if (resp.Estatus) {
        this.showModal = false;
        this.onGetUsuarios();
      }
    }, (error) =>{
      console.log(error.message);
    });
  }

  ngOnBuscar() {
    if (this.btnBuscar.length == 6) {
      this.btnBuscar = "Cancelar";
      this.txtBuscar = this.auxBuscar;
    } else {
      this.btnBuscar = "Buscar";
      this.txtBuscar = "";
      this.auxBuscar = "";
    }
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

  ngOnModalOpc(Item: any, Opcion: number): void {
    this.btnOpcion = Opcion;
    this.iSelect = Item;
    this.showModal = true;
  }

}
