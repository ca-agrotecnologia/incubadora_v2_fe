import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { AuthService } from 'src/app/_guards/auth.service';
import { TipoModel } from 'src/app/webapps/wrapper/models/tipo.model';
import { InstitucionModel } from '../../models/institucion.model';
import { RespModel } from '../../models/resp.model';

@Component({
  selector: 'app-cu-user',
  templateUrl: './cu-user.component.html',
  styleUrls: ['./cu-user.component.css'],
})
export class CuUserComponent implements OnInit {
  public formUsuarios!: FormGroup;
  /*
  public Carreras = Carrera;
  public Tipos = Tipo;
  public Turnos = Turno;
  public InstitucionID = InstitucionID;*/
  public Tipos = TipoModel;
  public instituciones: InstitucionModel[] = [];
  public modal = {
    titulo: '',
    msg: '',
  };
  public showModal = false;
  public sUsuario: any;
  public Opcion = 0;
  public uSession: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private uSer: UsersService,
    private aSer: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.uSession = this.aSer.onGetUsuario();
    this.route.params.subscribe((params) => {
      this.Opcion = params['Opcion'];
      if (params['Opcion'] == 4) {
        this.uSer
          .getUsuario(params['UsuarioID'], this.uSession.Token)
          .subscribe((resp: RespModel) => {
            if (resp.Estatus) {
              this.ngOnAsignarForm(resp.Data);
            }
          });
      }
      this.ngOnGetInstituciones();
    });
  }

  ngOnGetInstituciones(): void {
    this.uSer.getInstituciones().subscribe(
      (resp: RespModel) => {
        if (resp.Estatus) {
          this.instituciones = resp.Data;
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  private buildForm() {
    this.formUsuarios = this.formBuilder.group({
      UsuarioID: '',
      InstitucionID: [-1, [Validators.required]],
      Correo: ['', [Validators.required]],
      RepCorreo: ['', [Validators.required]],
      Clave: ['', [Validators.required]],
      RepClave: ['', [Validators.required]],
      Nombres: ['', [Validators.required]],
      Apellidos: ['', [Validators.required]],
      Telefono: ['', [Validators.required]],
      Tipo: [-1, [Validators.required]],
    });
  }

  onGuardarUsuario() {
    if (
      this.formUsuarios.value['InstitucionID'] != -1 &&
      this.formUsuarios.value['Turno'] != -1 &&
      this.formUsuarios.value['Tipo'] != -1
    ) {
      if (
        this.formUsuarios.value['Correo'] ===
        this.formUsuarios.value['RepCorreo']
      ) {
        if (
          this.formUsuarios.value['Clave'] ===
          this.formUsuarios.value['RepClave']
        ) {
          this.uSer
            .postUsuario({
              Token: this.uSession.Token,
              Usuario: this.formUsuarios.value,
            })
            .subscribe(
              (resp: RespModel) => {
                if (resp.Estatus) {
                  this.modal.titulo = 'Notificación...';
                  this.modal.msg = 'El usuario fue agregado al sistema.';
                  this.showModal = true;
                  this.formUsuarios.reset();
                } else {
                  this.modal.titulo = 'Notificación...';
                  this.modal.msg = 'El correo ya tiene registro de usuario';
                  this.showModal = true;
                }
              },
              (error) => {
                console.log(error.message);
              }
            );
        } else {
          this.modal.titulo = 'Error...';
          this.modal.msg = 'Las contraseñas no son iguales';
          this.showModal = true;
        }
      } else {
        this.modal.titulo = 'Error...';
        this.modal.msg = 'Los correos no son iguales';
        this.showModal = true;
      }
    } else {
      this.modal.titulo = 'Error...';
      this.modal.msg = 'Existen datos en blanco';
      this.showModal = true;
    }
  }

  onEditarUsuario() {
    if (
      this.formUsuarios.value['InstitucionID'] != -1 &&
      this.formUsuarios.value['Turno'] != -1 &&
      this.formUsuarios.value['Tipo'] != -1
    ) {
      if (
        this.formUsuarios.value['Correo'] ===
        this.formUsuarios.value['RepCorreo']
      ) {
        if (
          this.formUsuarios.value['Clave'] ===
          this.formUsuarios.value['RepClave']
        ) {
          console.log({
            Opcion: 2,
            Token: this.uSession.Token,
            Usuario: this.formUsuarios.value,
          });

          this.uSer
            .putEstatusUsuario({
              Opcion: 2,
              Token: this.uSession.Token,
              Usuario: this.formUsuarios.value,
            })
            .subscribe((resp: RespModel) => {
              if (resp.Estatus) {
                this.modal.titulo = 'Notificación...';
                this.modal.msg = 'Los cambios fueron aceptados';
                this.showModal = true;
              } else {
                this.modal.titulo = 'Notificación...';
                this.modal.msg = 'El correo ya tiene registro de usuario';
                this.showModal = true;
              }
            });
        } else {
          this.modal.titulo = 'Error...';
          this.modal.msg = 'Las contraseñas no son iguales';
          this.showModal = true;
        }
      } else {
        this.modal.titulo = 'Error...';
        this.modal.msg = 'Los correos no son iguales';
        this.showModal = true;
      }
    } else {
      this.modal.titulo = 'Error...';
      this.modal.msg = 'Existen datos en blanco';
      this.showModal = true;
    }
  }

  ngOnAsignarForm(item: any): void {

    this.formUsuarios.controls['UsuarioID'].setValue(item.UsuarioID);
    this.formUsuarios.controls['InstitucionID'].setValue(
      item.InstitucionID * 1
    );
    this.formUsuarios.controls['Correo'].setValue(item.Correo);
    this.formUsuarios.controls['RepCorreo'].setValue(item.Correo);
    this.formUsuarios.controls['Clave'].setValue(item.Clave);
    this.formUsuarios.controls['RepClave'].setValue(item.Clave);
    this.formUsuarios.controls['Nombres'].setValue(item.Nombres);
    this.formUsuarios.controls['Apellidos'].setValue(item.Apellidos);
    this.formUsuarios.controls['Telefono'].setValue(item.Telefono);
    this.formUsuarios.controls['Tipo'].setValue(item.Tipo * 1);
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }
}
