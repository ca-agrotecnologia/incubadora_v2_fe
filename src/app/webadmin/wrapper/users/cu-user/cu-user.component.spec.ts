import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuUserComponent } from './cu-user.component';

describe('CuUserComponent', () => {
  let component: CuUserComponent;
  let fixture: ComponentFixture<CuUserComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CuUserComponent]
    });
    fixture = TestBed.createComponent(CuUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
