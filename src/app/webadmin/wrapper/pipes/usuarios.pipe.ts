import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usuarios'
})
export class UsuariosPipe implements PipeTransform {

  transform(value: any, arg: string): any {

    if (arg === '' || arg.length < 3) {
        return value;
    }
    const resultado = [];
    for (const item of value) {
        if (item.Nombres.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
            resultado.push(item);
        }
    }
    return resultado;
}

}
