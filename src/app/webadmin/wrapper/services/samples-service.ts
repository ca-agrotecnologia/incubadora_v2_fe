import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespModel } from '../models/resp.model';

@Injectable({
  providedIn: 'root'
})
export class SamplesService {
  configUrl = 'http://localhost/incubadora_be/public/v2/webadmin/muestras';
  configUrlIncubadora = 'http://localhost/incubadora_be/public/v2/webadmin/incubadoras';
  headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });

  constructor(
    private http: HttpClient
  ) { }

  /**
   *
   * @param UsuarioID
   * @returns RespModel
   * Endpoint para la funcionalidad de muestras
   */
  getMuestras(UsuarioID: number) {
    return this.http.get <RespModel> (this.configUrl + '/todas/' + UsuarioID, { headers: this.headers });
  }

  getMuestra(MuestraID: number, Token: string) {
    return this.http.get <RespModel> (this.configUrl + '/' + MuestraID + '/' + Token, { headers: this.headers });
  }

  postMuestra(Muestra: any) {
    return this.http.post <RespModel> (this.configUrl , Muestra, { headers: this.headers });
  }

  putMuestra(Muestra: any) {
    return this.http.put <RespModel> (this.configUrl, Muestra, { headers: this.headers });
  }


  deleteMuestra(MuestraID: number, Token: string) {
    return this.http.delete <RespModel> (this.configUrl + '/' + MuestraID + '/' + Token, { headers: this.headers });
  }

  /**
   *
   * @param Institucion
   * @param Token
   * @returns RespMoel
   * EndPoints para la funcionalida de Incubadoras
   */
  getIncubadoras(Institucion: number, Token: string) {
    return this.http.get <RespModel> (this.configUrlIncubadora + '/institucion/' + Institucion + '/' + Token, { headers: this.headers });
  }

}
