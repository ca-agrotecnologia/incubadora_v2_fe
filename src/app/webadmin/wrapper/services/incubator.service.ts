import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespModel } from '../models/resp.model';

@Injectable({
  providedIn: 'root',
})
export class IncubatorService {
  configUrl = 'http://localhost/incubadora_be/public/v2/webadmin/incubadoras';

  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
  });

  constructor(private http: HttpClient) {}

  getIncubadoras(Token: string) {
    return this.http.get<RespModel>(this.configUrl + '/institucion/' + Token, {
      headers: this.headers,
    });
  }

  getIncubadora(IncubadoraID: number, Token: string) {
    return this.http.get<RespModel>(
      this.configUrl + '/' + IncubadoraID + '/' + Token,
      {
        headers: this.headers,
      }
    );
  }

  putEstatusIncubadora(Incubadora: any) {
    return this.http.put<RespModel>(this.configUrl + '', Incubadora, {
      headers: this.headers,
    });
  }
  postIncubadora(Incubadora: any) {
    return this.http.post<RespModel>(this.configUrl + '', Incubadora, {
      headers: this.headers,
    });
  }

  eliminarIncubadora(IncubadoraID: number, Token: string) {
    return this.http.delete <RespModel>(this.configUrl + '/' + IncubadoraID + '/' + Token, {
      headers: this.headers,
    });
  }
  /**
   *
   * @returns RepModel
   * obtiene el listado de instituciones con las que cuenta el sistema.
   */
  getInstituciones() {
    return this.http.get<RespModel>(this.configUrl + '/instituciones', {
      headers: this.headers,
    });
  }
}
