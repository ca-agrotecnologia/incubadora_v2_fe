import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespModel } from '../models/resp.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  configUrl = 'http://localhost/incubadora_be/public/v2/webadmin/usuarios';
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
  });

  constructor(private http: HttpClient) {}

  getUsuarios(InstitucionID: number, Token: string) {
    return this.http.get<RespModel>(
      this.configUrl + '/' + InstitucionID + '/' + Token,
      { headers: this.headers }
    );
  }

  getUsuario(UsuarioID: number, Token: string) {
    return this.http.get<RespModel>(this.configUrl + '/editar/' + UsuarioID + '/' + Token, {
      headers: this.headers,
    });
  }

  putEstatusUsuario(Usuario: any) {
    return this.http.put<RespModel>(this.configUrl, Usuario, {
      headers: this.headers,
    });
  }

  putTokenUsuario(Usuario: any) {
    return this.http.put<RespModel>(this.configUrl, Usuario, {
      headers: this.headers,
    });
  }

  postUsuario(Usuario: any) {
    return this.http.post<RespModel>(this.configUrl, Usuario, {
      headers: this.headers,
    });
  }

  eliminarUsuario(UsuarioID: number, Token: string) {
    return this.http.delete<RespModel>(
      this.configUrl + '/' + UsuarioID + '/' + Token,
      { headers: this.headers }
    );
  }


  getInstituciones() {
    return this.http.get <RespModel> (this.configUrl + '/instituciones', {
      headers: this.headers,
    });
  }
}
