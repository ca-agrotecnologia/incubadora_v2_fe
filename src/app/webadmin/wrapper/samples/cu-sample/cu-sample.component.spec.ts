import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuSampleComponent } from './cu-sample.component';

describe('CuSampleComponent', () => {
  let component: CuSampleComponent;
  let fixture: ComponentFixture<CuSampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CuSampleComponent]
    });
    fixture = TestBed.createComponent(CuSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
