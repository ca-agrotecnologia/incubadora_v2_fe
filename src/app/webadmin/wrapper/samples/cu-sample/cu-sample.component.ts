import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TipoModel } from 'src/app/webapps/wrapper/models/tipo.model';
import { InstitucionModel } from '../../models/institucion.model';
import { ActivatedRoute } from '@angular/router';
import { SamplesService } from '../../services/samples-service';
import { AuthService } from 'src/app/_guards/auth.service';
import { RespModel } from '../../models/resp.model';

@Component({
  selector: 'app-cu-sample',
  templateUrl: './cu-sample.component.html',
  styleUrls: ['./cu-sample.component.css']
})
export class CuSampleComponent implements OnInit {


  public formMuestra!: FormGroup;
  public incubadoras: any;
  public Tipos = TipoModel;
  public instituciones: InstitucionModel [] = [];
  public modal = {
    titulo: '',
    msg: ''
  };
  public showModal = false;
  public sUsuario: any;
  public Opcion = 0;
  public auxCorreo = '';
  public uSession: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private sSer: SamplesService,
    private aServicio: AuthService
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.uSession = this.aServicio.onGetUsuario();
    this.route.params.subscribe(params => {
      this.Opcion = params['Opcion'];
      if (params['Opcion'] == 3) {
        this.sSer.getMuestra(params['MuestraID'], this.uSession.Token).subscribe((resp: RespModel) => {
          if (resp.Estatus) {
            this.ngOnAsignarForm(resp.Data);
          }
        });
      }
    });
    this.sSer.getIncubadoras(this.uSession.Institucion, this.uSession.Token).subscribe((resp: RespModel) => {
      if (resp.Estatus) {
        this.incubadoras = resp.Data;
      }
    });
  }

  private buildForm(): void {
    this.formMuestra = this.formBuilder.group({
      MuestraID: '',
      IncubadoraID: '-1',
      UsuarioID: '',
      Nombre: ['', [Validators.required]],
      Descripcion: ['', [Validators.required]],
      Codigo: ['', [Validators.required]],
      Hora: ['', [Validators.required]],
      Tiempo: ['', [Validators.required]],
      Tipo: ['', [Validators.required]]
    });
  }

  onGuardarMuestra(): void {
    if (this.formMuestra.value['IncubadoraID'] != -1) {
      this.formMuestra.controls['UsuarioID'].setValue(this.uSession.UsuarioID);
      this.sSer.postMuestra({ Token: this.uSession.Token, Muestra: this.formMuestra.value }).subscribe((resp: RespModel) => {
        if (resp.Estatus) {
          this.modal.titulo = "Notificación...";
          this.modal.msg = resp.Data;
          this.showModal = true;
          this.formMuestra.reset();
          this.formMuestra.controls['IncubadoraID'].setValue(-1);
        }
      }, (error)=>{
        console.log(error.message);
      });
    } else {
      this.modal.titulo = "Error...";
      this.modal.msg = "Existen datos en blanco";
      this.showModal = true;
    }
  }

  onEditarMuestra(): void {
    if (this.formMuestra.value['IncubadoraID'] != -1) {
      this.sSer.putMuestra({ Opcion: 1, Token: this.uSession.Token, Muestra: this.formMuestra.value }).subscribe((resp: RespModel) => {
        if (resp.Estatus) {
          this.modal.titulo = "Notificación...";
          this.modal.msg = resp.Data;
          this.showModal = true;
        } else {
          this.modal.titulo = "Notificación...";
          this.modal.msg = resp.Data;
          this.showModal = true;
        }
      });
    } else {
      this.modal.titulo = "Error...";
      this.modal.msg = "Existen datos en blanco";
      this.showModal = true;
    }
  }

  ngOnAsignarForm(item: any): void {
    this.formMuestra.controls['MuestraID'].setValue(item.MuestraID);
    this.formMuestra.controls['IncubadoraID'].setValue(item.IncubadoraID);
    this.formMuestra.controls['UsuarioID'].setValue(item.UsuarioID);
    this.formMuestra.controls['Nombre'].setValue(item.Nombre);
    this.formMuestra.controls['Descripcion'].setValue(item.Descripcion);
    this.formMuestra.controls['Codigo'].setValue(item.Codigo);
    this.formMuestra.controls['Hora'].setValue(item.Hora);
    this.formMuestra.controls['Tiempo'].setValue(item.Tiempo);
    this.formMuestra.controls['Tipo'].setValue(item.Tipo);
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

}
