import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_guards/auth.service';
import { SamplesService } from '../services/samples-service';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-samples',
  templateUrl: './samples.component.html',
  styleUrls: ['./samples.component.css']
})
export class SamplesComponent implements OnInit {
  public fecha = new Date;
  public showModal = false;
  public muestras: any;
  public uSession: any;
  public iSelect: any;
  public btnOpcion = 0;
  public pageSize = 5;
  public page: number = 1;
  public btnBuscar: string = "Buscar";
  public txtBuscar: string = "";
  public auxBuscar: string = "";
  public numMuestras!: number;

  constructor(
    private iSer: SamplesService,
    private aServicio: AuthService
  ) { }

  ngOnInit(): void {
    this.uSession = this.aServicio.onGetUsuario();
    this.onGetMuestras();
  }

  onGetMuestras(): void {
    this.iSer.getMuestras(this.uSession.UsuarioID).subscribe((resp: RespModel) => {
      if (resp.Estatus) {
        this.muestras = resp.Data;
        this.numMuestras = this.muestras.length;
      }
    });
  }

  onCambiarPermiso() {
    this.iSelect.Estatus = this.iSelect.Estatus == 1 ? 0: 1;
    this.iSer.putMuestra({ Opcion: 0, Token: this.uSession.Token, Muestra: this.iSelect }).subscribe((resp: RespModel) => {
      if (resp.Estatus) {
        this.showModal = false;
        this.onGetMuestras();
      }
    });
  }

  onEliminarUsuario() {
    this.iSer.deleteMuestra(this.iSelect.MuestraID, this.uSession.Token).subscribe((resp: RespModel) => {
      console.log(resp);
      if (resp.Estatus) {
        this.showModal = false;
        this.onGetMuestras();
      }
    }, (error)=>{
      console.log(error.message);
    });
  }

  ngOnBuscar() {
    if (this.btnBuscar.length == 6) {
      this.btnBuscar = "Cancelar";
      this.txtBuscar = this.auxBuscar;
    } else {
      this.btnBuscar = "Buscar";
      this.txtBuscar = "";
      this.auxBuscar = "";
    }
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

  ngOnModalOpc(Item: any, Opcion: number): void {
    this.btnOpcion = Opcion;
    this.iSelect = Item;
    this.showModal = true;
  }
}
