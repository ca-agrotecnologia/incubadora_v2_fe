import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/_guards/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public NRuta!: string;
  public sUsuario: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private aServicio: AuthService
    ) {
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          this.NRuta = this.router.url;
        }
      }
    );
  }

  ngOnInit(): void {
    this.sUsuario = this.aServicio.onGetUsuario();
  }
}
