import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WrapperComponent } from './wrapper/wrapper.component';
import { SamplesComponent } from './wrapper/samples/samples.component';
import { UsersComponent } from './wrapper/users/users.component';
import { IncubatorComponent } from './wrapper/incubator/incubator.component';
import { ProfilesComponent } from './wrapper/profiles/profiles.component';
import { DashboardComponent } from './wrapper/dashboard/dashboard.component';
import { CuSampleComponent } from './wrapper/samples/cu-sample/cu-sample.component';
import { CuIncubatorComponent } from './wrapper/incubator/cu-incubator/cu-incubator.component';
import { CuUserComponent } from './wrapper/users/cu-user/cu-user.component';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    children: [
      {
        path: 'panel',
        component: DashboardComponent,
      },
      {
        path: 'muestras',
        component: SamplesComponent,
      },
      {
        path: 'muestras/:Opcion/:MuestraID',
        component: CuSampleComponent,
      },
      {
        path: 'usuarios',
        component: UsersComponent,
      },
      {
        path: 'usuarios/:Opcion/:UsuarioID',
        component: CuUserComponent,
      },
      {
        path: 'incubadoras',
        component: IncubatorComponent,
      },

      {
        path: 'incubadoras/:Opcion/:IncubadoraID',
        component: CuIncubatorComponent,
      },
      {
        path: 'perfil',
        component: ProfilesComponent,
      },
      {
        path: '**',
        redirectTo: 'muestras',
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebadminRouting {}
