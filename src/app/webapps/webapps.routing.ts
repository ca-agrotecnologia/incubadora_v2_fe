import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WrapperComponent } from './wrapper/wrapper.component';
import { HomeComponent } from './wrapper/home/home.component';
import { IncubatorsComponent } from './wrapper/incubators/incubators.component';
import { LoginComponent } from './wrapper/login/login.component';
import { CreatorsComponent } from './wrapper/creators/creators.component';
import { RegisterComponent } from './wrapper/register/register.component';
import { RecoveryComponent } from './wrapper/recovery/recovery.component';
import { SamplesComponent } from './wrapper/incubators/samples/samples.component';
import { ChartsComponent } from './wrapper/incubators/samples/charts/charts.component';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    children: [
        {
          path: 'inicio',
          component: HomeComponent
        },
        {
          path: 'incubadoras',
          component: IncubatorsComponent
        },
        {
          path: 'incubadoras/muestras/:IncubadoraID',
          component: SamplesComponent
        },
        {
          path: 'incubadoras/muestras/graficas/:MuestraID',
          component: ChartsComponent
        },
        {
            path: 'acceso',
            component: LoginComponent
        },
        {
            path: 'creadores',
            component: CreatorsComponent
        },
        {
            path: 'registro',
            component: RegisterComponent
        },
        {
            path: 'recuperar',
            component: RecoveryComponent
        },
        {
            path:'**',
            redirectTo: 'inicio'
        }
    ]
},
{
    path: '**',
    redirectTo: ''
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebappsRouting { }
