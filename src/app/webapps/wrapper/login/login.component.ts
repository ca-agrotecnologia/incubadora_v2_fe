import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppsService } from '../services/apps.service';
import { AuthService } from 'src/app/_guards/auth.service';
import { InstitucionModel } from '../models/institucion.model';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public formLogin!: FormGroup;
  public instituciones: InstitucionModel [] = [];
  public auxCorreo!: string;
  public modal = {
    titulo: '',
    msg: '',
  };
  public showModal = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private appsSer: AppsService,
    private aSer: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.ngOnGetInstituciones();
  }
  ngOnGetInstituciones(): void {
    this.appsSer.getInstituciones().subscribe(
      (resp: RespModel) => {
        if (resp.Estatus) {
          this.instituciones = resp.Data;
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  private buildForm(): void {
    this.formLogin = this.formBuilder.group({
      InstitucionID: -1,
      Correo_Nom_Usu: ['', [Validators.required]],
      Clave: ['', [Validators.required]],
    });
  }

  ngOnEntrar(): void {
    if (this.formLogin.value['Institucion'] !== -1) {
      this.appsSer.postAcceso(this.formLogin.value).subscribe((resp: RespModel) => {
        if (resp.Estatus) {
          if (this.aSer.onSetUsuario(resp.Data)) {
            this.router.navigate(['admin']);
          }
        } else {
          this.modal.titulo = 'Error...';
          this.modal.msg = resp.Data;
          this.showModal = true;
          this.formLogin.reset();
          this.formLogin.controls['InstitucionID'].setValue(-1);
        }
      }, (error)=>{
        console.log(error.message);

      });
    } else {
      this.modal.titulo = 'Notificación';
      this.modal.msg =
        'Favor de revisar los campos solicitados, falta información.';
      this.showModal = true;
      this.formLogin.reset();
      //this.formLogin.controls.InstitucionID.setValue(-1);
    }
  }

  toggleModal(): void  {
    this.showModal = !this.showModal;
  }
}
