import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespModel } from '../models/resp.model';

@Injectable({
  providedIn: 'root',
})
export class AppsService {
  configUrl = 'http://localhost/incubadora_be/public/v2/webapps/';
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
  });

  constructor(private http: HttpClient) {}

  getIncubadoras() {
    return this.http.get(this.configUrl + 'incubadoras', {
      headers: this.headers,
    });
  }

  getIncubadoraMuestras(MuestraID: number) {
    return this.http.get(this.configUrl + 'muestras/' + MuestraID, {
      headers: this.headers,
    });
  }

  getIncubadoraMuestrasGraficas(MuestraID: any) {
    return this.http.get(this.configUrl + 'muestras/graficas/' + MuestraID, {
      headers: this.headers,
    });
  }
  getIncubadoraMuestraSeleccionada(MuestraID: number) {
    return this.http.get(this.configUrl + 'incubadora/muestra/' + MuestraID, {
      headers: this.headers,
    });
  }

  setMuestrasGraficasNotas(Notas: any) {
    return this.http.post(this.configUrl + 'muestras/graficas/notas', Notas, {
      headers: this.headers,
    });
  }

  setMuestrasGraficasNotasEliminar(Nota: any) {
    return this.http.delete(
      this.configUrl + 'muestras/graficas/notas/eliminar',
      { headers: this.headers, body: Nota }
    );
  }

  getInstituciones() {
    return this.http.get <RespModel> (this.configUrl + 'instituciones', {
      headers: this.headers,
    });
  }

  postAcceso(Datos: any) {
    return this.http.post <RespModel> (this.configUrl + 'login', Datos, {
      headers: this.headers,
    });
  }

  postRecuperar(Datos: any) {
    return this.http.post <RespModel> (this.configUrl + 'recuperar', Datos, {
      headers: this.headers,
    });
  }

  postRegistro(Datos: any) {
    return this.http.post <RespModel> (this.configUrl + 'registrar', Datos, {
      headers: this.headers,
    });
  }
}
