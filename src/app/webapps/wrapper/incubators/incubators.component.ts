import { Component, OnInit } from '@angular/core';
import { AppsService } from '../services/apps.service';

@Component({
  selector: 'app-incubators',
  templateUrl: './incubators.component.html',
  styleUrls: ['./incubators.component.css']
})
export class IncubatorsComponent implements OnInit {

  public incubadoras: any;
  constructor (
    private aSer: AppsService
  ) {}

  ngOnInit(): void {
    this.ngOnGetIncubators();
  }

  ngOnGetIncubators(): void {
    this.aSer.getIncubadoras().subscribe((resp: any) => {
      if (resp.Estatus) {
        this.incubadoras = resp.Data;
      }
    },(error) => {
      console.log(error);
    });
  }

}
