import { Component, OnInit } from '@angular/core';
import { AppsService } from '../../../services/apps.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css'],
})
export class ChartsComponent implements OnInit {
  public opcionesGrafica: any;
  public xAxisData: string[] = [];
  public dataT: number[] = [];
  public MuestraID: number = -1;
  public Monitoreos: any;
  public Monitoreo: any;
  public Muestra: any;
  public showModal = false;

  public formNota!: FormGroup;

  public OpcAE = -1;
  public NotaEliminar: any;
  public Notas: any;
  public formEliminarNota!: FormGroup;

  constructor(
    private aSer: AppsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.buildForm();
    this.route.params.subscribe((params) => {
      this.MuestraID = params['MuestraID'];
      this.aSer
        .getIncubadoraMuestraSeleccionada(params['MuestraID'])
        .subscribe((resp: any) => {
          if (resp.Estatus) {
            this.Muestra = resp.Data;
            this.onGetGraficas(params['MuestraID']);
          }
        });
    });
  }
  onRecargarGrafica(): void {
    this.onGetGraficas(this.MuestraID);
  }
  onGetGraficas(MuestraID: any) {
    this.xAxisData = [];
    this.dataT = [];
    this.aSer.getIncubadoraMuestrasGraficas(MuestraID).subscribe(
      (resp: any) => {
        console.log(resp);

        if (resp.Estatus) {
          this.Monitoreos = resp.Data;
          this.Notas = resp.Notas;
          for (let item of resp.Data) {
            this.onGrafica(item.Temperatura, item.Hora);
          }
          this.opcionesGrafica = {
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'cross',
                label: {
                  backgroundColor: '#6a0085',
                },
              },
            },
            legend: {
              data: ['Temperatura'],
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true,
            },
            xAxis: [
              {
                type: 'category',
                boundaryGap: false,
                data: this.xAxisData,
              },
            ],
            yAxis: [
              {
                type: 'value',
              },
            ],
            series: [
              {
                name: 'Temperatura',
                type: 'line',
                stack: 'counts',
                label: {
                  normal: {
                    show: true,
                    position: 'top',
                  },
                },
                areaStyle: { normal: {} },
                data: this.dataT,
              },
            ],
          };
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  onGrafica(TemperaturaReal: number, Hora: string) {
    this.dataT.push(TemperaturaReal);
    this.xAxisData.push(Hora);
  }

  buildForm() {
    this.formNota = this.formBuilder.group({
      Clave: ['', [Validators.required]],
      Nota: ['', [Validators.required]],
    });
    this.formEliminarNota = this.formBuilder.group({
      Codigo: ['', [Validators.required]],
    });
  }

  onChartClick(event: { dataIndex: string | number }) {
    this.showModal = true;
    this.Monitoreo = this.Monitoreos[event.dataIndex];
    this.OpcAE = 0;
  }

  ngOnRegistrarNota(): void {
    const { Clave, Nota } = this.formNota.value;
    this.aSer
      .setMuestrasGraficasNotas({
        MonitoreoID: this.Monitoreo.MonitoreoID,
        Codigo: Clave,
        Nota: Nota,
        Hora: this.Monitoreo.Hora,
        Temperatura: this.Monitoreo.Temperatura,
      })
      .subscribe((resp: any) => {
        if (resp.Estatus) {
          this.formNota.reset();
          this.onGetGraficas(this.MuestraID);
          this.showModal = false;
        }
      });
  }

  ngOnSeleccionarNota(Nota: any): void {
    this.NotaEliminar = Nota;
    this.showModal = true;
    this.OpcAE = 1;
  }
  ngOnEliminarNota(): void {
    const { Codigo } = this.formEliminarNota.value;
    const MuestraID = this.MuestraID;
    const NotaID = this.NotaEliminar.NotaID;

    this.aSer
      .setMuestrasGraficasNotasEliminar({
        MuestraID: MuestraID,
        NotaID: NotaID,
        Codigo: Codigo,
      })
      .subscribe(
        (resp: any) => {
          if (resp.Estatus) {
            this.formEliminarNota.reset();
            this.onGetGraficas(this.MuestraID);
            this.showModal = false;
          }
        },
        (error) => {
          console.log(error);
          this.showModal = false;
        }
      );
  }

  toggleModal() {
    this.showModal = !this.showModal;
    this.formNota.reset();
  }
}
