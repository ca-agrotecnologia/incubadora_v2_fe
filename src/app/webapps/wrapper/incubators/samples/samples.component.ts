import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppsService } from '../../services/apps.service';

@Component({
  selector: 'app-samples',
  templateUrl: './samples.component.html',
  styleUrls: ['./samples.component.css']
})
export class SamplesComponent implements OnInit {
  private IncubadoraID: any;
  public Muestras: any;
  public pageSize = 5;
  public page: number = 1;
  public btnBuscar: string = "Buscar";
  public txtBuscar: string = "";
  public auxBuscar: string = "";


  constructor (
    private route: ActivatedRoute,
    private aServ: AppsService
  ) {}
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.IncubadoraID = params['IncubadoraID'];
      this.onGetIncubadoraMuestras(params['IncubadoraID']);
    });
  }
  onGetIncubadoraMuestras(IncubadoraID: any) {
    this.aServ.getIncubadoraMuestras(IncubadoraID).subscribe((resp: any) => {
      if (resp.Estatus) {
        this.Muestras = resp.Data;
      }
    }, (error)=> {
      console.log(error);
    });
  }
  ngOnBuscar() {
    if (this.btnBuscar.length == 6) {
      this.btnBuscar = "Cancelar";
      this.txtBuscar = this.auxBuscar;
    } else {
      this.btnBuscar = "Buscar";
      this.txtBuscar = "";
      this.auxBuscar = "";
    }

  }

}
