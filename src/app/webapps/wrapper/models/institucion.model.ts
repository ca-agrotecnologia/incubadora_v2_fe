export interface InstitucionModel {
  InstitucionID : number;
  Nombre: string;
  Sistemas: string;
  Telefono : string;
  Director : string;
  Domicilio : string;
  Colonia : string;
  Municipio : string;
  Estado : string;
  Referencia : string;
  Estatus	: boolean;
  FechAlta : string;

}
