export const TipoModel = [
  {
      Id: -1,
      Nombre: 'Tipo de usuario'
  },
  {
      Id: 0,
      Nombre: 'SuperAdministrador'
  },
  {
      Id: 1,
      Nombre: 'Administrador'
  },
  {
      Id: 2,
      Nombre: 'Administrativo'
  },
  {
      Id: 3,
      Nombre: 'Docente'
  },
  {
      Id: 4,
      Nombre: 'Alumno'
  }
];
