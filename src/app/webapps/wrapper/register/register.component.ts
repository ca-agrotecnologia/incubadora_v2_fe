import { Component } from '@angular/core';
import { InstitucionModel } from '../models/institucion.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppsService } from '../services/apps.service';
import { TipoModel } from '../models/tipo.model';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  public tipos = TipoModel;
  public instituciones: InstitucionModel [] = [];
  public openTab:number = 1;
  public formRegistro!: FormGroup;
  public modal = {
    titulo: '',
    msg: ''
  };
  public showModal = false;

  constructor(
    private formBuilder: FormBuilder,
    private appsSer: AppsService
  ) {

  }

  ngOnInit(): void {
    this.builForm();
    this.ngOnGetInstituciones();
  }
  ngOnGetInstituciones(): void {
    this.appsSer.getInstituciones().subscribe(
      (resp: RespModel) => {
        if (resp.Estatus) {
          this.instituciones = resp.Data;
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  private builForm(): void {
    this.formRegistro = this.formBuilder.group({
      InstitucionID: -1,
      Tipo: -1,
      Correo: ['', [Validators.required]],
      rCorreo: ['', [Validators.required]],
      Clave: ['', [Validators.required, Validators.minLength(6)]],
      rClave: ['', [Validators.required, Validators.minLength(6)]],
      Nombres: ['', [Validators.required]],
      Apellidos: ['', [Validators.required]],
      Telefono: ['', [Validators.required]]
    });
  }


  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
  }
  ngOnRegistro(): void {
    if (this.formRegistro.value['InstitucionID'] != -1 && this.formRegistro.value['Carrera'] != -1 && this.formRegistro.value['Tipo'] != -1) {
      if (this.formRegistro.value['Correo'] === this.formRegistro.value['rCorreo']) {
        if (this.formRegistro.value['Clave'] === this.formRegistro.value['rClave']) {
          this.appsSer.postRegistro(this.formRegistro.value).subscribe((resp: RespModel) => {
            if (resp.Estatus) {
              this.modal.titulo = "Notificación";
              this.modal.msg = resp.Data;
              this.showModal = true;
              this.formRegistro.reset();
              this.formRegistro.controls['InstitucionID'].setValue(-1);
              this.formRegistro.controls['Tipo'].setValue(-1);
            } else {
              this.modal.titulo = "Notificación";
              this.modal.msg = resp.Data;
              this.showModal = true;
            }
          });
        } else {
          this.modal.titulo = "Notificación";
          this.modal.msg = 'Las claves no son iguales, comprobarlo.';
          this.showModal = true;
        }

      } else {
        this.modal.titulo = "Notificación";
        this.modal.msg = 'Los correos no coinciden favor de comprobarlo.';
        this.showModal = true;
      }
    } else {
      this.modal.titulo = "Notificación";
      this.modal.msg = 'Favor de revisar los campos solicitados, falta información.';
      this.showModal = true;
    }
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

}
