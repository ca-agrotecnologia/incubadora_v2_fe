import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppsService } from '../services/apps.service';
import { InstitucionModel } from '../models/institucion.model';
import { RespModel } from '../models/resp.model';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.css']
})
export class RecoveryComponent {

  public formRecuperar!: FormGroup;
  public instituciones: InstitucionModel [] = [];
  public modal = {
    titulo: '',
    msg: ''
  };
  public showModal = false;

  constructor(
    private formBuilder: FormBuilder,
    private appsSer: AppsService
  ) { }

  ngOnInit(): void {
    this.builForm();
    this.ngOnGetInstituciones();
  }

  private builForm(): void {
    this.formRecuperar = this.formBuilder.group({
      InstitucionID: -1,
      Correo: ['', [Validators.required]],
    });
  }

  ngOnGetInstituciones(): void {
    this.appsSer.getInstituciones().subscribe(
      (resp: RespModel) => {
        if (resp.Estatus) {
          this.instituciones = resp.Data;
        }
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  ngOnRecuperar(): void {
    if (this.formRecuperar.value['InstitucionID'] != -1) {
      this.appsSer.postRecuperar(this.formRecuperar.value).subscribe((resp: RespModel) => {
        if (resp.Data) {
          this.modal.titulo = "Notificacion";
          this.modal.msg = resp.Data;
          this.showModal = true;
          this.formRecuperar.reset();
          this.formRecuperar.controls['InstitucionID'].setValue(-1);

        } else {
          this.modal.titulo = "Error...";
          this.modal.msg = resp.Data;
          this.showModal = true;
        }
      });
    } else {
      this.modal.titulo = "Error...";
      this.modal.msg = 'Favor de revisar los campos solicitados, falta información.';
      this.showModal = true;
    }
  }


  toggleModal() {
    this.showModal = !this.showModal;
  }

}
