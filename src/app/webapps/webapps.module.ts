import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WrapperComponent } from './wrapper/wrapper.component';
import { NavbarComponent } from './wrapper/navbar/navbar.component';
import { FooterComponent } from './wrapper/footer/footer.component';
import { HomeComponent } from './wrapper/home/home.component';
import { LoginComponent } from './wrapper/login/login.component';
import { RegisterComponent } from './wrapper/register/register.component';
import { RecoveryComponent } from './wrapper/recovery/recovery.component';
import { CreatorsComponent } from './wrapper/creators/creators.component';
import { IncubatorsComponent } from './wrapper/incubators/incubators.component';
import { SamplesComponent } from './wrapper/incubators/samples/samples.component';
import { ChartsComponent } from './wrapper/incubators/samples/charts/charts.component';
import { NotFoundComponent } from './wrapper/not-found/not-found.component';
import { WebappsRouting } from './webapps.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { MuestrasPipe } from './wrapper/pipes/muestras.pipe';
import { NgxEchartsModule } from 'ngx-echarts';




@NgModule({
  declarations: [
    WrapperComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    RecoveryComponent,
    CreatorsComponent,
    IncubatorsComponent,
    SamplesComponent,
    ChartsComponent,
    NotFoundComponent,
    MuestrasPipe
  ],
  imports: [
    CommonModule,
    WebappsRouting,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })

  ]
})
export class WebappsModule { }
